﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ReferralTrigger
{
    
    public interface IDataRepository<T> where T : class
    {
        IQueryable<T> All();
        T Find(object id);
        void Insert(T entity);
        void Delete(T entityToDelete);
        IQueryable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties);
        void Edit(T entityToUpdate);
        void ExecuteCommand(String command);
        LiveQv3CustomerDataEntities GetContext();
    }

    public class DataRepository<T> : IDataRepository<T> where T : class
    {
        private LiveQv3CustomerDataEntities _db;

        public DataRepository(LiveQv3CustomerDataEntities context)
        {
            _db = context;
        }

        public LiveQv3CustomerDataEntities GetContext()
        {
            return _db;
        }

        public IQueryable<T> All()
        {
            return _db.Set<T>();
        }

        public IQueryable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = _db.Set<T>();
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public virtual T Find(object id)
        {
            return _db.Set<T>().Find(id);
        }

        public virtual void Insert(T entity)
        {
            _db.Set<T>().Add(entity);
            _db.SaveChanges();
        }

        public virtual void Delete(T entityToDelete)
        {
            if (_db.Entry(entityToDelete).State == EntityState.Detached)
            {
                _db.Set<T>().Attach(entityToDelete);
            }
            _db.Set<T>().Remove(entityToDelete);
            _db.SaveChanges();
        }

        public void Edit(T entityToUpdate)
        {
            //if (_db.Entry(entityToUpdate).State == EntityState.Modified)
            //{
            //    _db.Set<T>().Attach(entityToUpdate);
            //}
            _db.Entry(entityToUpdate).State = EntityState.Modified;
            //_db.Set<T>().AddOrUpdate(entityToUpdate);
            _db.SaveChanges();
        }

        //Does this mean we have to have a repository for every ReportViewModel?
        //Do we want the ReportViewModels to be able to update and delete? Is there any harm?
        //public virtual List<T> SqlQueryToList(string query)
        //{
        //    var result = _db.Database.SqlQuery<T>(query).ToList();
        //    return result;
        //}

        public void ExecuteCommand(String command)
        {
            _db.Database.ExecuteSqlCommand(command);
        }
    }
}
