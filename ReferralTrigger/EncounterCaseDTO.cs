﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReferralTrigger
{
    public class EncounterCaseDTO
    {
        public EncounterCaseDTO(int caseId, string guid)
        {
            lhCaseId = caseId;
            lookupGUID = guid;
        }

        public EncounterCaseDTO()
        {
        }

        public int lhCaseId { get; set; }

        public string lookupGUID { get; set; }

        public string encounterType { get; set; }
    }
}
