﻿using Cranmore.DVA.LiveQ.ReferralTrigger.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReferralTrigger.VehicleTypeFilters
{
    class TaxiInfringementFilter : IInfringementFilter
    {
        private IInfringementActionService _infringementActionService;

        public TaxiInfringementFilter(IInfringementActionService infringementActionService)
        {
            _infringementActionService = infringementActionService;
        }

        public List<EncounterCaseDTO> FilterEncountersByInfringements(List<EncounterCaseDTO> lhCaseId)
        {
            return _infringementActionService.FilterCasesWithSpecificInfringments(lhCaseId);
        }
    }
}
