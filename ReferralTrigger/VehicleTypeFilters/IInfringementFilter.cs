﻿using Cranmore.DVA.LiveQ.ReferralTrigger.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReferralTrigger.VehicleTypeFilters
{
    interface IInfringementFilter
    {
        List<EncounterCaseDTO> FilterEncountersByInfringements(List<EncounterCaseDTO> lhCaseId);
    }
}
