﻿using Cranmore.DVA.LiveQ.ReferralTrigger.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReferralTrigger.VehicleTypeFilters
{
    public class InfringementFilter
    {
        private TaxiInfringementFilter _taxiFilter;
        private BusInfringementFilter _busFilter;
        private GoodsInfringementFilter _goodsFilter;

        public InfringementFilter(IInfringementActionService infringementActionService)
        {
            _busFilter = new BusInfringementFilter(infringementActionService);
            _taxiFilter = new TaxiInfringementFilter(infringementActionService);
            _goodsFilter = new GoodsInfringementFilter(infringementActionService);
        }

        public List<EncounterCaseDTO> FilterByInfringements(List<EncounterCaseDTO> encounterDtos)
        {
            List<EncounterCaseDTO> filteredDTOs = new List<EncounterCaseDTO>();

            foreach(var vehicleEncounterDTO in encounterDtos.GroupBy(a => a.encounterType))
            {
                switch (vehicleEncounterDTO.Key)
                {
                    case "Goods":
                        filteredDTOs.AddRange(_goodsFilter.FilterEncountersByInfringements(encounterDtos));
                        break;
                    case "Taxi":
                        filteredDTOs.AddRange(_taxiFilter.FilterEncountersByInfringements(encounterDtos));
                        break;
                    case "Bus":
                        filteredDTOs.AddRange(_busFilter.FilterEncountersByInfringements(encounterDtos));
                        break;
                }
            }

            return filteredDTOs;
        }
    }
}
