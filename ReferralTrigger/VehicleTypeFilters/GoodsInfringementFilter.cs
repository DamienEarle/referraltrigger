﻿using Cranmore.DVA.LiveQ.ReferralTrigger.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReferralTrigger.VehicleTypeFilters
{
    class GoodsInfringementFilter : IInfringementFilter
    {
        private IInfringementActionService _infringementActionService;

        public GoodsInfringementFilter(IInfringementActionService infringementActionService)
        {
            _infringementActionService = infringementActionService;
        }

        public List<EncounterCaseDTO> FilterEncountersByInfringements(List<EncounterCaseDTO> lhCaseId)
        {
            return _infringementActionService.FilterCasesWithMSIorVSI(lhCaseId);
        }
    }
}
