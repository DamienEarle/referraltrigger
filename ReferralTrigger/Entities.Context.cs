﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ReferralTrigger
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class LiveQv3CustomerDataEntities : DbContext
    {
        public LiveQv3CustomerDataEntities()
            : base("name=LiveQv3CustomerDataEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<Tbl_fn_Infringements_Infringement_Actions> Tbl_fn_Infringements_Infringement_Actions { get; set; }
        public DbSet<Tbl_fn_VRM_Lookup_VRM_Lookup> Tbl_fn_VRM_Lookup_VRM_Lookup { get; set; }
        public DbSet<Tbl_fn_VRM_Lookup_Vehicles> Tbl_fn_VRM_Lookup_Vehicles { get; set; }
    }
}
