﻿using Cranmore.DVA.LiveQ.ReferralTrigger.Services;
using Cranmore.Briefcase.Repo.Models;
using Cranmore.Briefcase.Repo.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReferralTrigger;
using ReferralTrigger.VehicleTypeFilters;

namespace Cranmore.DVA.LiveQ.ReferralTrigger
{
    public class ReferralTrigger
    {
        private ICaseLinkService _caseLinkService;
        private ICaseTypeService _caseTypeService;
        private ICaseService _caseService;
        private Ilh_CasesService _lhCaseService;
        private ICaseCreator _caseCreator;
        private ICaseIdGenerationService _caseIdGenerationService;
        private IVRMLookupService _vrmLookupService;
        private IInfringementActionService _infringementActionService;
        private ILHCaseDataService _lhCaseDataService;
        private InfringementFilter _infringementFilter;

        private readonly static int _referralCaseTypeId = 55;
        private readonly string operationStatus = "Completed";
        private readonly string operationReferredStatus = "Operation Encounter Referred";
        private readonly string encounterCompleteStatus = "Encounter Complete";
        private readonly int caseTypeId = 51;

        private readonly WorkflowActionObject CreateCaseDTO = new WorkflowActionObject()
        {
            NewCaseTypeId = 55, NewCaseStatus = "New"
        };


        public ReferralTrigger(ICaseService caseService, Ilh_CasesService lhCaseService, IVRMLookupService vrmLookupService, ICaseLinkService caseLinkService,
            ICaseIdGenerationService caseIdGenerationService, ICaseTypeService caseTypeService, IInfringementActionService infringementActionService,
            ILHCaseDataService lhCaseDataService)
        {
            _caseLinkService = caseLinkService;
            _caseTypeService = caseTypeService;
            _caseService = caseService;
            _caseIdGenerationService = caseIdGenerationService;
            _vrmLookupService = vrmLookupService;
            _infringementActionService = infringementActionService;
            _lhCaseService = lhCaseService;
            _lhCaseDataService = lhCaseDataService;
            _infringementFilter = new InfringementFilter(infringementActionService);

            _caseCreator = new CaseCreator(_caseTypeService, _caseService, _caseIdGenerationService, _caseLinkService);
        }

        public List<Case> GetCompletedOperations()
        {
            return _caseService.GetCaseByCaseTypeIdAndStatus(caseTypeId, operationStatus);
        }

        public lh_Cases GetOperationLhCase(Case operation)
        {
            return _lhCaseService.GetByCaseName(operation.CaseName);
        }

        public List<string> GetEncounterGUIDSFromOperation(lh_Cases lhCase)
        {
            return _vrmLookupService.GetEncounterGUIDsForOperation(lhCase.lh_CasesId);
        }

        public List<EncounterCaseDTO> FilterLhCasesWhereComplete(List<EncounterCaseDTO> encounterDtos)
        {
            return encounterDtos.Where(id => EncounterIsComplete(id.lhCaseId)).ToList();
        }

        public List<EncounterCaseDTO> FilterLhCasesWithInfringement(List<EncounterCaseDTO> encounterDtos)
        {
            return _infringementFilter.FilterByInfringements(encounterDtos);
        }

        private bool HasNoReferral(int lhCaseId)
        {
            var linkedCases = _caseLinkService.GetCaseLinkByLhCaseId(lhCaseId);
            foreach (var caseLink in linkedCases)
            {
                var linkedCase = _caseService.GetCaseById(caseLink.CaseId);
                if (linkedCase.CaseTypeId == _referralCaseTypeId)
                    return false;
            }

            return true;
        }

        private bool EncounterIsComplete(int lhCaseId)
        {
            return _lhCaseService.GetByCaseId(lhCaseId).Status == encounterCompleteStatus;
        }

        public List<EncounterCaseDTO> BuildEncounterDTOsFromLookupGuids(List<string> lookupGuids)
        {
            var encounterDtos = new List<EncounterCaseDTO>();
            foreach (var lookupGuid in lookupGuids)
            {
                var linkoutGuid = _lhCaseDataService.GetCaseGUIDContainingPartialGUID(lookupGuid);
                if (string.IsNullOrEmpty(linkoutGuid))
                    continue;

                var encounterDTO = BuildEncounterDtofromGuids(lookupGuid, linkoutGuid);

                if (encounterDTO.lhCaseId != 0)
                    encounterDtos.Add(encounterDTO);
            }

            return encounterDtos;
        }

        private EncounterCaseDTO BuildEncounterDtofromGuids(string lookupGuid, string linkoutGuid)
        {
            var encounterDTO = new EncounterCaseDTO()
            {
                lhCaseId = _lhCaseService.GetCaseIdContainingPartialGUID(linkoutGuid),
                lookupGUID = lookupGuid,
                encounterType = GetEncounterTypeFromGUID(lookupGuid),
            };
            return encounterDTO;
        }

        public Case CreateNewReferralCase()
        {
            return _caseCreator.CreateNewCase(CreateCaseDTO);
        }

        public string GetEncounterTypeFromGUID(string GUID)
        {
            return _vrmLookupService.GetEncounterType(GUID);
        }

        public void UpdateOperationStatus(Case operation)
        {
            operation.CaseStatus = operationReferredStatus;
            _caseService.Update(operation);
        }
    }
}


