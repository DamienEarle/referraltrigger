﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cranmore.Briefcase.Repo.Interfaces;
using Cranmore.Briefcase.Repo.Models;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using ReferralTrigger;

namespace Cranmore.DVA.LiveQ.ReferralTrigger.Services
{
    public interface IInfringementActionService
    {
        List<EncounterCaseDTO> FilterCasesWithMSIorVSI(List<EncounterCaseDTO> encounterCases);

        List<EncounterCaseDTO> FilterCasesWithSpecificInfringments(List<EncounterCaseDTO> encounterCases);
    }

    public class InfringementActionService : IInfringementActionService
    {
        private IDataRepository<Tbl_fn_Infringements_Infringement_Actions> _repo;

        public InfringementActionService(IDataRepository<Tbl_fn_Infringements_Infringement_Actions> caseRepository)
        {
            _repo = caseRepository;
        }

        public List<EncounterCaseDTO> FilterCasesWithMSIorVSI(List<EncounterCaseDTO> encounterCases)
        {
            List<int> ids = encounterCases.Select(a => a.lhCaseId).ToList();

            List<int> validIds = _repo.All().Join(ids.ToList(), 
                x => x.CaseId, y => y, (x, y) => x)
                .Where(a=>a.Category_25 == "MSI" || a.Category_25 == "VSI")
                .Select(a=>a.CaseId).ToList();
            
            var dtos = new List<EncounterCaseDTO>();
            foreach (var encountermap in encounterCases)
            {
                if (validIds.Contains(encountermap.lhCaseId))
                    dtos.Add(encountermap);
            }

            return dtos;
        }

        public List<EncounterCaseDTO> FilterCasesWithSpecificInfringments(List<EncounterCaseDTO> encounterCases)
        {
            List<int> ids = encounterCases.Select(a => a.lhCaseId).ToList();

            List<int> validIds = _repo.All().Join(ids.ToList(),
                x => x.CaseId, y => y, (x, y) => x)
                .Where(action => (action.Driver_Action_4 == "Prosecution" || action.Driver_Action_4 == "Fixed Penalty"
                || action.Operator_Action_6 == "Prosecution" || action.Operator_Action_6 == "Fixed Penalty"
                || action.User_Action_10 == "Prosecution" || action.User_Action_10 == "Fixed Penalty"
                || action.Owner_Action_8 == "Prosecution" || action.Owner_Action_8 == "Fixed Penalty")
                && ((action.Question_29 == "Can TDL be produced" && action.Answer_30 == "No")
                || (action.Question_29 == "What is the Operator Licence Infringement " && action.Answer_30 == "Use Unlicensed Drivers")
                || (action.Question_29 == "Operator operating a taxi service with unlicenced driver" && action.Answer_30 == "Yes")))
                .Select(a => a.CaseId).ToList();

            var dtos = new List<EncounterCaseDTO>();
            foreach (var encountermap in encounterCases)
            {
                if (validIds.Contains(encountermap.lhCaseId))
                    dtos.Add(encountermap);
            }

            return dtos;
        }
    }
}
