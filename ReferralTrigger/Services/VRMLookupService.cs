﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cranmore.Briefcase.Repo.Interfaces;
using Cranmore.Briefcase.Repo.Models;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using ReferralTrigger;

namespace Cranmore.DVA.LiveQ.ReferralTrigger.Services
{
    public interface IVRMLookupService
    {
        List<string> GetEncounterGUIDsForOperation(int caseId);

        string GetEncounterType(string GUID);
    }

    public class VRMLookupService : IVRMLookupService
    {
        private IDataRepository<Tbl_fn_VRM_Lookup_Vehicles> _repo;

        public VRMLookupService(IDataRepository<Tbl_fn_VRM_Lookup_Vehicles> caseRepository)
        {
            _repo = caseRepository;
        }

        public List<string> GetEncounterGUIDsForOperation(int caseId)
        {
            var lookups = _repo.All().Where(a => a.CaseId == caseId);
            return GetValidGUIDSFromLookups(lookups);
        }

        private List<string> GetValidGUIDSFromLookups(IEnumerable<Tbl_fn_VRM_Lookup_Vehicles> lookups)
        {
            var EncounterGUIDs = new List<string>();

            foreach (var lookup in lookups)
            {
                var linkOutGuid = String.Concat(lookup.Start_New_Bus_Encounter_26,
                    lookup.Start_New_Car_Encounter_34,
                    lookup.Start_New_Goods_Encounter_25,
                    lookup.Start_New_Taxi_Encounter_27,
                    lookup.Start_Quick_Bus_Encounter_41,
                    lookup.Start_Quick_Car_Encounter_42,
                    lookup.Start_Quick_Goods_Encounter_44,
                    lookup.Start_Quick_Taxi_Encounter_38);

                if (!string.IsNullOrEmpty(linkOutGuid))
                    EncounterGUIDs.Add(linkOutGuid);
            }
            return EncounterGUIDs;
        }

        public string GetEncounterType(string GUID)
        {
            if(_repo.All().FirstOrDefault(a=>a.Start_New_Bus_Encounter_26 == GUID 
                || a.Start_Quick_Bus_Encounter_41 == GUID) != null)
            {
                return "Bus";
            }
            if (_repo.All().FirstOrDefault(a => a.Start_New_Goods_Encounter_25 == GUID 
                || a.Start_Quick_Goods_Encounter_44 == GUID) != null)
            {
                return "Goods";
            }
            if (_repo.All().FirstOrDefault(a => a.Start_New_Taxi_Encounter_27 == GUID 
                || a.Start_Quick_Taxi_Encounter_38 == GUID) != null)
            {
                return "Taxi";
            }

            return null;
        }

    }
}
