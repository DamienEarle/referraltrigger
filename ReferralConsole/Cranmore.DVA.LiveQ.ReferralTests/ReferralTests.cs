﻿using Cranmore.Briefcase.Repo.Interfaces;
using Cranmore.Briefcase.Repo.Models;
using Cranmore.Briefcase.Repo.Services;
using Cranmore.DVA.LiveQ.ReferralTrigger;
using Cranmore.LiveQ.SchemaService;
using Cranmore.DVA.LiveQ.ReferralTrigger.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ReferralTrigger;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Cranmore.DVA.LiveQ.ReferralTests
{

    [TestClass]
    public class ReferralTriggerTests
    {
        private Mock<BriefcaseEntities> _mockContext;
        private Mock<LiveQv3CustomerDataEntities> _mockDataContext;

        private CaseService _caseService;
        private lh_CasesService _lhCaseService;
        private VRMLookupService _vrmLookupService;
        private CaseLinkService _caseLinkService;
        private CaseIdGenerationService _caseIdGenerationService;
        private CaseTypeService _caseTypeService;
        private FormsService _formService;
        private CaseDataService _caseDataService;
        private InfringementActionService _infringementActionService;
        private lh_CaseDataService _lhCaseDataService;

        private SchemaService _schemaService;
        private ReferralTrigger.ReferralTrigger _target;

        [TestInitialize]
        public void SetupTest()
        {
            SetupCaseService();
            SetupLhCaseService();
            SetupVRMLookupService();
            SetupCaseLinksService();
            SetupCaseGenerationService();
            SetupCaseTypeService();
            SetupFormService();
            SetupCaseDataService();
            SetupInfringementActionService();
            SetupLHCaseDataService();

            _target = new ReferralTrigger.ReferralTrigger(_caseService, _lhCaseService, _vrmLookupService, 
                _caseLinkService, _caseIdGenerationService, _caseTypeService, _infringementActionService, _lhCaseDataService);

            _schemaService = new Briefcase.Repo.Services.SchemaService(_caseService, _caseDataService, _formService, _caseLinkService);
        }

        private void SetupCaseService()
        {
            var data = new List<Case>
            {
                new Case(){CaseId = 1, CaseTypeId = 51, CaseStatus = "Completed", CaseName = "CaseName1"},
                new Case(){CaseId = 2, CaseTypeId = 51, CaseStatus = "Completed", CaseName = "CaseName2"},
                new Case(){CaseId = 3, CaseTypeId = 51, CaseStatus = "In Progress", CaseName = "CaseName3"},
                new Case(){CaseId = 4, CaseTypeId = 51, CaseStatus = "In progress", CaseName = "CaseName4"},
                new Case(){CaseId = 5, CaseTypeId = 49, CaseStatus = "Audited"},
                new Case(){CaseId = 6, CaseTypeId = 49, CaseStatus = "Audited"},
                new Case(){CaseId = 7, CaseTypeId = 55, CaseStatus = "New"},
                new Case(){CaseId = 8, CaseTypeId = 55, CaseStatus = "New"},
                new Case(){CaseId = 9, CaseTypeId = 55, CaseStatus = "New"},
            }.AsQueryable();
            
            var mockSet = new Mock<DbSet<Case>>();

            mockSet.As<IQueryable<Case>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<Case>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<Case>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<Case>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            _mockContext = new Mock<BriefcaseEntities>();
            _mockContext.Setup(x => x.Set<Case>()).Returns(mockSet.Object);
            IRepository<Case> _mockRepo = new Repository<Case>(_mockContext.Object);

            _caseService = new CaseService(_mockRepo);
        }

        private void SetupLhCaseService()
        {
            var data = new List<lh_Cases>
            {
                new lh_Cases(){lh_CasesId = 1, CaseName = "CaseName1", CaseGUID = "OPERATION1"},
                new lh_Cases(){lh_CasesId = 2, CaseName = "CaseName2", CaseGUID = "OPERATION1"},
                new lh_Cases(){lh_CasesId = 3, CaseName = "CaseName3", CaseGUID = "OPERATION1"},
                new lh_Cases(){lh_CasesId = 4, CaseName = "CaseName4", CaseGUID = "OPERATION1-GUID1"},
                new lh_Cases(){lh_CasesId = 5, CaseName = "CaseName5", CaseGUID = "OPERATION1-GUID2"},
                new lh_Cases(){lh_CasesId = 6, CaseName = "CaseName5", CaseGUID = "OPERATION1-GUID3"}
            }.AsQueryable();

            var mockSet = new Mock<DbSet<lh_Cases>>();

            mockSet.As<IQueryable<lh_Cases>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<lh_Cases>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<lh_Cases>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<lh_Cases>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            _mockContext = new Mock<BriefcaseEntities>();
            _mockContext.Setup(x => x.Set<lh_Cases>()).Returns(mockSet.Object);
            IRepository<lh_Cases> _mockRepo = new Repository<lh_Cases>(_mockContext.Object);

            _lhCaseService = new lh_CasesService(_mockRepo);
        }

        private void SetupVRMLookupService()
        {
            var data = new List<Tbl_fn_VRM_Lookup_Vehicles>
            {
                new Tbl_fn_VRM_Lookup_Vehicles(){CaseId = 1, Start_New_Bus_Encounter_26 = "GUID1", Start_Quick_Taxi_Encounter_38 = "", Start_Quick_Goods_Encounter_44 = ""},
                new Tbl_fn_VRM_Lookup_Vehicles(){CaseId = 1, Start_New_Bus_Encounter_26 = "GUID2", Start_Quick_Taxi_Encounter_38 = "", Start_Quick_Goods_Encounter_44 = ""},
                new Tbl_fn_VRM_Lookup_Vehicles(){CaseId = 1, Start_New_Bus_Encounter_26 = "", Start_Quick_Taxi_Encounter_38 = "GUID3", Start_Quick_Goods_Encounter_44 = ""},
                new Tbl_fn_VRM_Lookup_Vehicles(){CaseId = 2, Start_New_Goods_Encounter_25 = "GUID4", Start_Quick_Taxi_Encounter_38 = "", Start_Quick_Goods_Encounter_44 = ""},
                new Tbl_fn_VRM_Lookup_Vehicles(){CaseId = 3, Start_Quick_Bus_Encounter_41 = "", Start_New_Goods_Encounter_25 = "GUID5", Start_New_Taxi_Encounter_27 = ""},
                new Tbl_fn_VRM_Lookup_Vehicles(){CaseId = 4, Start_New_Bus_Encounter_26 = "", Start_Quick_Taxi_Encounter_38 = "", Start_Quick_Goods_Encounter_44 = "GUID6"},
            }.AsQueryable();

            var mockSet = new Mock<DbSet<Tbl_fn_VRM_Lookup_Vehicles>>();

            mockSet.As<IQueryable<Tbl_fn_VRM_Lookup_Vehicles>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<Tbl_fn_VRM_Lookup_Vehicles>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<Tbl_fn_VRM_Lookup_Vehicles>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<Tbl_fn_VRM_Lookup_Vehicles>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            _mockDataContext = new Mock<LiveQv3CustomerDataEntities>();
            _mockDataContext.Setup(x => x.Set<Tbl_fn_VRM_Lookup_Vehicles>()).Returns(mockSet.Object);
            IDataRepository<Tbl_fn_VRM_Lookup_Vehicles> _mockRepo = new DataRepository<Tbl_fn_VRM_Lookup_Vehicles>(_mockDataContext.Object);

            _vrmLookupService = new VRMLookupService(_mockRepo);
        }

        private void SetupCaseLinksService()
        {
            var data = new List<CaseLinks>
            {
                new CaseLinks(){ CaseLinkId = 1, CaseId = 1, LinkedCaseId = 2 },
                new CaseLinks(){ CaseLinkId = 2, CaseId = 1, LinkedCaseId = 2 },
                new CaseLinks(){ CaseLinkId = 3, CaseId = 2, LinkedCaseId = 3 },
                new CaseLinks(){ CaseLinkId = 4, CaseId = 2, LinkedCaseId = 3 },
                new CaseLinks(){ CaseLinkId = 5, CaseId = 3, LinkedCaseId = 4 },
                new CaseLinks(){ CaseLinkId = 6, CaseId = 7, LinkedCaseId = 4 },
                new CaseLinks(){ CaseLinkId = 7, CaseId = 8, LinkedLhCaseId = 4 }
            }.AsQueryable();

            var mockSet = new Mock<DbSet<CaseLinks>>();

            mockSet.As<IQueryable<CaseLinks>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<CaseLinks>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<CaseLinks>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<CaseLinks>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            _mockContext = new Mock<BriefcaseEntities>();
            _mockContext.Setup(x => x.Set<CaseLinks>()).Returns(mockSet.Object);
            IRepository<CaseLinks> _mockRepo = new Repository<CaseLinks>(_mockContext.Object);

            _caseLinkService = new CaseLinkService(_mockRepo);
        }

        private void SetupCaseTypeService()
        {
            var data = new List<CaseType>
            {
                new CaseType(){CaseTypeId = 55, AllowAutoCaseIdGeneration = false, CaseIdPattern = "yyyymm/RI/xxxxx" },
                new CaseType(){CaseTypeId = 51, AllowAutoCaseIdGeneration = false, CaseIdPattern = "yyyymm/RO/xxxxx" }
            }.AsQueryable();

            var mockSet = new Mock<DbSet<CaseType>>();

            mockSet.As<IQueryable<CaseType>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<CaseType>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<CaseType>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<CaseType>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            _mockContext = new Mock<BriefcaseEntities>();
            _mockContext.Setup(x => x.Set<CaseType>()).Returns(mockSet.Object);
            IRepository<CaseType> _mockRepo = new Repository<CaseType>(_mockContext.Object);

            _caseTypeService = new CaseTypeService(_mockRepo);
        }

        private void SetupCaseGenerationService()
        {
            var data = new List<TempCaseIdentifier>
            {
                new TempCaseIdentifier(){ }
            }.AsQueryable();

            var mockSet = new Mock<DbSet<TempCaseIdentifier>>();

            mockSet.As<IQueryable<TempCaseIdentifier>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<TempCaseIdentifier>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<TempCaseIdentifier>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<TempCaseIdentifier>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            _mockContext = new Mock<BriefcaseEntities>();
            _mockContext.Setup(x => x.Set<TempCaseIdentifier>()).Returns(mockSet.Object);
            IRepository<TempCaseIdentifier> _mockRepo = new Repository<TempCaseIdentifier>(_mockContext.Object);

            _caseIdGenerationService = new CaseIdGenerationService(_mockRepo);
        }

        private void SetupFormService()
        {
            var data = new List<Forms>
            {
                new Forms() { FormId = 1, JSON = @"[{""id"":0,""parent_id"":null,""order"":1,""attributes"":{""caption"":""Create Regulatory Case"",""FormId"":77,""NCFormId"":70,""Name"":""Create Regulatory Case"",""Type"":""Form"",""Factory"":""DVA"",""Area"":""1"",""Function"":""DE Notes"",""Version"":1,""Status"":""DRAFT"",""RevisionDate"":""28/01/2016 11:49:57"",""parameters"":[""JOBID"",""USERID"",""GPS""],""appmode"":""false"",""buttons"":""true"",""next_caption"":""Next"",""prev_caption"":""Previous"",""casestatuschange"":""Open"",""partiallysaved"":""false""},""pictures"":{""parameters"":{}},""name"":""form"",""title"":""Form"",""caption"":""Create Regulatory Case"",""rules"":[],""data_queries"":[],""uuid"":null,""nc"":false,""job_id"":"""",""isVisible"":true,""rows"":[]},{""id"":1,""parent_id"":0,""order"":1,""attributes"":{""caption"":""New Regulatory Case"",""type"":""linear"",""maptodb"":""true"",""layout"":""vertical"",""add_caption"":""Add to table"",""totals"":""false"",""datetime"":""true"",""allowdel"":""false"",""allowedit"":""false"",""timer"":""0"",""rowrequired"":""true"",""hastablebeenchanged"":""false""},""pictures"":{},""name"":""section"",""title"":""Section"",""caption"":""New Regulatory Case"",""rules"":[],""data_queries"":[],""uuid"":null,""nc"":false,""job_id"":"""",""isVisible"":true,""rows"":[],""totals"":[]},{""id"":2,""parent_id"":1,""order"":3,""attributes"":{""caption"":""Vehicle Type"",""options"":[{""option"":""Goods"",""value"":""Goods"",""code"":[],""severity"":[],""raise_nc"":false},{""option"":""Taxi"",""value"":""Taxi"",""code"":[],""severity"":[],""raise_nc"":false},{""option"":""Bus"",""value"":""Bus"",""code"":[],""severity"":[],""raise_nc"":false}],""required"":""true"",""buttons"":""true"",""severity"":""Minor"",""sendnotifcation"":""false"",""assigncase"":""false"",""casestatusupdate"":""false"",""maptodb"":""true""},""pictures"":{""options"":{}},""name"":""list"",""title"":""Dropdown Field"",""caption"":""Vehicle Type"",""rules"":[],""data_queries"":[],""uuid"":null,""nc"":false,""job_id"":"""",""isVisible"":true,""rows"":[]},{""id"":3,""parent_id"":1,""order"":4,""attributes"":{""caption"":""Case Type"",""options"":[{""option"":""Compliance Audit"",""value"":""Compliance Audit"",""code"":[],""severity"":[],""raise_nc"":false},{""option"":""Operating Centre Assessment"",""value"":""Operating Centre Assessment"",""code"":[],""severity"":[],""raise_nc"":false},{""option"":""Referral"",""value"":""Referral"",""code"":[],""severity"":[],""raise_nc"":false},{""option"":""TDL Pick up"",""value"":""TDL Pick up"",""code"":[],""severity"":[],""raise_nc"":false}],""required"":""true"",""buttons"":""true"",""severity"":""Minor"",""sendnotifcation"":""false"",""assigncase"":""false"",""casestatusupdate"":""false"",""maptodb"":""true""},""pictures"":{""options"":{}},""name"":""list"",""title"":""Dropdown Field"",""caption"":""Case Type"",""rules"":[{""statements"":[{""type"":""Clause"",""field_id"":2,""operator"":""NE"",""test"":""Taxi"",""coltest"":""""}],""actions"":[{""key"":""SET_ATTR"",""field_id"":3,""set_type"":""Text"",""attribute_name"":""options"",""set_from_attribute_name"":"""",""value"":""Compliance Audit,Operating Centre Assessment,Referral"",""formula"":null}]},{""statements"":[{""type"":""Clause"",""field_id"":2,""operator"":""EQ"",""test"":""Taxi"",""coltest"":""""}],""actions"":[{""key"":""SET_ATTR"",""field_id"":null,""set_type"":""Text"",""attribute_name"":""options"",""set_from_attribute_name"":"""",""value"":""Compliance Audit,Operating Centre Assessment,Referral, TDL Pick up"",""formula"":null}]}],""data_queries"":[],""uuid"":null,""nc"":false,""job_id"":"""",""isVisible"":true,""rows"":[]},{""id"":4,""parent_id"":1,""order"":0,""attributes"":{""caption"":""User"",""required"":""true"",""hidden"":""true"",""default"":""USERNAME"",""buttons"":""true"",""iscasename"":""false"",""casestatusupdate"":""false"",""maptodb"":""true""},""pictures"":{},""name"":""varchar"",""title"":""Text Field"",""caption"":""User"",""rules"":[],""data_queries"":[],""uuid"":null,""nc"":false,""job_id"":"""",""isVisible"":false,""rows"":[]},{""id"":5,""parent_id"":1,""order"":2,""attributes"":{""caption"":""Date received"",""helptext"":""Please enter a date as dd/mm/yyyy"",""required"":""true"",""validregex"":""DATE_SYSTEM"",""example"":""25/12/2013"",""severity"":""Minor"",""maptodb"":""true""},""pictures"":{},""name"":""datetime"",""title"":""Date Field"",""caption"":""Date received"",""rules"":[],""data_queries"":[],""uuid"":null,""nc"":false,""job_id"":"""",""isVisible"":true,""rows"":[]},{""id"":6,""parent_id"":1,""order"":1,""attributes"":{""caption"":""Date"",""required"":""true"",""hidden"":""true"",""default"":""DATE"",""buttons"":""true"",""iscasename"":""false"",""casestatusupdate"":""false"",""maptodb"":""true""},""pictures"":{},""name"":""varchar"",""title"":""Text Field"",""caption"":""Date"",""rules"":[],""data_queries"":[],""uuid"":null,""nc"":false,""job_id"":"""",""isVisible"":false,""rows"":[]}]" }
            }.AsQueryable();

            var mockSet = new Mock<DbSet<Forms>>();

            mockSet.As<IQueryable<Forms>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<Forms>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<Forms>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<Forms>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            _mockContext = new Mock<BriefcaseEntities>();
            _mockContext.Setup(x => x.Set<Forms>()).Returns(mockSet.Object);
            IRepository<Forms> _mockRepo = new Repository<Forms>(_mockContext.Object);

            _formService = new FormsService(_mockRepo);
        }

        private void SetupCaseDataService()
        {
            var data = new List<CaseData>
            {
                new CaseData()
            }.AsQueryable();

            var mockSet = new Mock<DbSet<CaseData>>();

            mockSet.As<IQueryable<CaseData>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<CaseData>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<CaseData>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<CaseData>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            _mockContext = new Mock<BriefcaseEntities>();
            _mockContext.Setup(x => x.Set<CaseData>()).Returns(mockSet.Object);
            IRepository<CaseData> _mockRepo = new Repository<CaseData>(_mockContext.Object);

            _caseDataService = new CaseDataService(_mockRepo);
        }

        private void SetupInfringementActionService()
        {
            var data = new List<Tbl_fn_Infringements_Infringement_Actions>
            {
                new Tbl_fn_Infringements_Infringement_Actions() { CaseId = 5, Category_25= "VSI"},
                new Tbl_fn_Infringements_Infringement_Actions() { CaseId = 6, Category_25= "MSI"},
                new Tbl_fn_Infringements_Infringement_Actions() { CaseId = 7, Category_25= "SI"}

            }.AsQueryable();

            var mockSet = new Mock<DbSet<Tbl_fn_Infringements_Infringement_Actions>>();

            mockSet.As<IQueryable<Tbl_fn_Infringements_Infringement_Actions>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<Tbl_fn_Infringements_Infringement_Actions>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<Tbl_fn_Infringements_Infringement_Actions>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<Tbl_fn_Infringements_Infringement_Actions>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            _mockDataContext = new Mock<LiveQv3CustomerDataEntities>();
            _mockDataContext.Setup(x => x.Set<Tbl_fn_Infringements_Infringement_Actions>()).Returns(mockSet.Object);
            IDataRepository<Tbl_fn_Infringements_Infringement_Actions> _mockRepo = new DataRepository<Tbl_fn_Infringements_Infringement_Actions>(_mockDataContext.Object);

            _infringementActionService = new InfringementActionService(_mockRepo);
        }

        private void SetupLhCaseDataService()
        {
            var data = new List<lh_CaseData>
            {
                new lh_CaseData() {lh_CaseDataId = 1 },
                new lh_CaseData() {lh_CaseDataId = 2 },
                new lh_CaseData() {lh_CaseDataId = 3 }

            }.AsQueryable();

            var mockSet = new Mock<DbSet<lh_CaseData>>();

            mockSet.As<IQueryable<lh_CaseData>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<lh_CaseData>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<lh_CaseData>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<lh_CaseData>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            _mockContext = new Mock<BriefcaseEntities>();
            _mockDataContext.Setup(x => x.Set<lh_CaseData>()).Returns(mockSet.Object);
            IDataRepository<lh_CaseData> _mockRepo = new DataRepository<lh_CaseData>(_mockDataContext.Object);

            //_lhCaseDataService = new lh_CaseDataService(_mockContext);
        }


        [TestMethod]
        public void GetCompletedOperations_Cases_AssertCompleted()
        {
            var operationStatus = "Completed";
            var caseTypeId = 51;
            
            var result = _target.GetCompletedOperations();

            Assert.AreEqual(true, result.All(a=>a.CaseStatus == operationStatus 
                && a.CaseTypeId == caseTypeId));
        }

        [TestMethod]
        public void GetCompletedOperations_Cases_VerifyCount()
        {
            var expectedCaseCount = 2;

            var result = _target.GetCompletedOperations();

            Assert.AreEqual(expectedCaseCount, result.Count());
        }

        [TestMethod]
        public void GetOperationLhCase()
        {
            var expectedCaseId = 1;

            var operation = _target.GetCompletedOperations().First();
            var result = _target.GetOperationLhCase(operation);

            Assert.AreEqual(expectedCaseId, result.lh_CasesId);
        }

        [TestMethod]
        public void GetEncounterGUIDSFromOperation_ListGuids()
        {
            var operation = _target.GetCompletedOperations().First();
            var lhCase = _target.GetOperationLhCase(operation);

            var result = _target.GetEncounterGUIDSFromOperation(lhCase);

            Assert.AreEqual(3, result.Count());
            Assert.AreEqual(true, result.All(a => !String.IsNullOrEmpty(a)));
        }

        [TestMethod]
        public void GetLhCaseIdsFromEncounterGuids()
        {
            var listOfGUIDs = new List<string>() { "GUID1", "GUID2", "GUID3" };

            var result = _target.BuildEncounterDTOsFromLookupGuids(listOfGUIDs);

            var expectedIds = new List<int>() { 4, 5, 6 };
            Assert.AreEqual(true, result.SequenceEqual(expectedIds));
        }

        [TestMethod]
        public void FilterLhCasesWithReferral()
        {
            var lhCaseIDsToFilter = new List<EncounterCaseDTO> {
            };

            var result = _target.FilterLhCasesWithReferral(lhCaseIDsToFilter);

            var expectedIds = new List<int>() { 5, 6 };
            Assert.AreEqual(true, result.SequenceEqual(expectedIds));
        }

        [TestMethod]
        public void FilterLhCasesWithInfringement_ExpectingOneMSI()
        {
            var lhCaseIDsToFilter = new List<int>() { 5, 4 };

            var result = _target.FilterLhCasesWithInfringement(lhCaseIDsToFilter);

            var expectedIds = new List<int>() { 5 };
            Assert.AreEqual(true, result.SequenceEqual(expectedIds));
        }

        [TestMethod]
        public void FilterLhCasesWithInfringement_ExpectingOneVSI()
        {
            var lhCaseIDsToFilter = new List<int>() { 6, 4 };

            var result = _target.FilterLhCasesWithInfringement(lhCaseIDsToFilter);

            var expectedIds = new List<int>() { 6 };
            Assert.AreEqual(true, result.SequenceEqual(expectedIds));
        }

        [TestMethod]
        public void CreateNewCase()
        {
            var result = _target.CreateNewReferralCase();

            Assert.AreNotEqual(null, result);
        }

        [TestMethod]
        public void GetFormJSON()
        {
            int formId = 1;

            var json = _schemaService.LoadFormJSON(formId);

            string expectedJSON = @"[{""id"":0,""parent_id"":null,""order"":1,""attributes"":{""caption"":""Create Regulatory Case"",""FormId"":77,""NCFormId"":70,""Name"":""Create Regulatory Case"",""Type"":""Form"",""Factory"":""DVA"",""Area"":""1"",""Function"":""DE Notes"",""Version"":1,""Status"":""DRAFT"",""RevisionDate"":""28/01/2016 11:49:57"",""parameters"":[""JOBID"",""USERID"",""GPS""],""appmode"":""false"",""buttons"":""true"",""next_caption"":""Next"",""prev_caption"":""Previous"",""casestatuschange"":""Open"",""partiallysaved"":""false""},""pictures"":{""parameters"":{}},""name"":""form"",""title"":""Form"",""caption"":""Create Regulatory Case"",""rules"":[],""data_queries"":[],""uuid"":null,""nc"":false,""job_id"":"""",""isVisible"":true,""rows"":[]},{""id"":1,""parent_id"":0,""order"":1,""attributes"":{""caption"":""New Regulatory Case"",""type"":""linear"",""maptodb"":""true"",""layout"":""vertical"",""add_caption"":""Add to table"",""totals"":""false"",""datetime"":""true"",""allowdel"":""false"",""allowedit"":""false"",""timer"":""0"",""rowrequired"":""true"",""hastablebeenchanged"":""false""},""pictures"":{},""name"":""section"",""title"":""Section"",""caption"":""New Regulatory Case"",""rules"":[],""data_queries"":[],""uuid"":null,""nc"":false,""job_id"":"""",""isVisible"":true,""rows"":[],""totals"":[]},{""id"":2,""parent_id"":1,""order"":3,""attributes"":{""caption"":""Vehicle Type"",""options"":[{""option"":""Goods"",""value"":""Goods"",""code"":[],""severity"":[],""raise_nc"":false},{""option"":""Taxi"",""value"":""Taxi"",""code"":[],""severity"":[],""raise_nc"":false},{""option"":""Bus"",""value"":""Bus"",""code"":[],""severity"":[],""raise_nc"":false}],""required"":""true"",""buttons"":""true"",""severity"":""Minor"",""sendnotifcation"":""false"",""assigncase"":""false"",""casestatusupdate"":""false"",""maptodb"":""true""},""pictures"":{""options"":{}},""name"":""list"",""title"":""Dropdown Field"",""caption"":""Vehicle Type"",""rules"":[],""data_queries"":[],""uuid"":null,""nc"":false,""job_id"":"""",""isVisible"":true,""rows"":[]},{""id"":3,""parent_id"":1,""order"":4,""attributes"":{""caption"":""Case Type"",""options"":[{""option"":""Compliance Audit"",""value"":""Compliance Audit"",""code"":[],""severity"":[],""raise_nc"":false},{""option"":""Operating Centre Assessment"",""value"":""Operating Centre Assessment"",""code"":[],""severity"":[],""raise_nc"":false},{""option"":""Referral"",""value"":""Referral"",""code"":[],""severity"":[],""raise_nc"":false},{""option"":""TDL Pick up"",""value"":""TDL Pick up"",""code"":[],""severity"":[],""raise_nc"":false}],""required"":""true"",""buttons"":""true"",""severity"":""Minor"",""sendnotifcation"":""false"",""assigncase"":""false"",""casestatusupdate"":""false"",""maptodb"":""true""},""pictures"":{""options"":{}},""name"":""list"",""title"":""Dropdown Field"",""caption"":""Case Type"",""rules"":[{""statements"":[{""type"":""Clause"",""field_id"":2,""operator"":""NE"",""test"":""Taxi"",""coltest"":""""}],""actions"":[{""key"":""SET_ATTR"",""field_id"":3,""set_type"":""Text"",""attribute_name"":""options"",""set_from_attribute_name"":"""",""value"":""Compliance Audit,Operating Centre Assessment,Referral"",""formula"":null}]},{""statements"":[{""type"":""Clause"",""field_id"":2,""operator"":""EQ"",""test"":""Taxi"",""coltest"":""""}],""actions"":[{""key"":""SET_ATTR"",""field_id"":null,""set_type"":""Text"",""attribute_name"":""options"",""set_from_attribute_name"":"""",""value"":""Compliance Audit,Operating Centre Assessment,Referral, TDL Pick up"",""formula"":null}]}],""data_queries"":[],""uuid"":null,""nc"":false,""job_id"":"""",""isVisible"":true,""rows"":[]},{""id"":4,""parent_id"":1,""order"":0,""attributes"":{""caption"":""User"",""required"":""true"",""hidden"":""true"",""default"":""USERNAME"",""buttons"":""true"",""iscasename"":""false"",""casestatusupdate"":""false"",""maptodb"":""true""},""pictures"":{},""name"":""varchar"",""title"":""Text Field"",""caption"":""User"",""rules"":[],""data_queries"":[],""uuid"":null,""nc"":false,""job_id"":"""",""isVisible"":false,""rows"":[]},{""id"":5,""parent_id"":1,""order"":2,""attributes"":{""caption"":""Date received"",""helptext"":""Please enter a date as dd/mm/yyyy"",""required"":""true"",""validregex"":""DATE_SYSTEM"",""example"":""25/12/2013"",""severity"":""Minor"",""maptodb"":""true""},""pictures"":{},""name"":""datetime"",""title"":""Date Field"",""caption"":""Date received"",""rules"":[],""data_queries"":[],""uuid"":null,""nc"":false,""job_id"":"""",""isVisible"":true,""rows"":[]},{""id"":6,""parent_id"":1,""order"":1,""attributes"":{""caption"":""Date"",""required"":""true"",""hidden"":""true"",""default"":""DATE"",""buttons"":""true"",""iscasename"":""false"",""casestatusupdate"":""false"",""maptodb"":""true""},""pictures"":{},""name"":""varchar"",""title"":""Text Field"",""caption"":""Date"",""rules"":[],""data_queries"":[],""uuid"":null,""nc"":false,""job_id"":"""",""isVisible"":false,""rows"":[]}]";
            Assert.AreEqual(expectedJSON, json);
        }

        [TestMethod]
        public void PopulateJSONForm()
        {
            int formId = 1;
            var json = _schemaService.LoadFormJSON(formId);

            var keyValueMap = new Dictionary<string, string>()
            {
                { "3", "Referral" },
                { "2", "Vehicle Type" }
            };

            var newJSON = _schemaService.PopulateJSONForm(keyValueMap, json);
           
            var jsonObjects = JsonConvert.DeserializeObject<List<dynamic>>(newJSON);

            Assert.AreEqual("Vehicle Type", jsonObjects.ElementAt(2).value.ToString());
            Assert.AreEqual("Referral", jsonObjects.ElementAt(3).value.ToString());
        }

        [TestMethod]
        public void LinkEncounterToNewCase()
        {
            var newCase = new Case() {CaseId = 10};
            var encounterLhCase = _lhCaseService.GetAll().First(a => a.lh_CasesId == 5);

            _schemaService.LinkEncounterToNewCase(newCase.CaseId, encounterLhCase.lh_CasesId);

            var linkedEncounters = _caseLinkService.GetCaseLinkByCaseId(10);
            Assert.AreEqual(1, linkedEncounters.Count());
        }

        [TestMethod]
        public void GetEncounterVehicleType_Bus()
        {
            var newCase = new Case() { CaseId = 10 };
            var busEncounterGUID = "GUID1";

            var result = _target.GetEncounterTypeFromGUID(busEncounterGUID);

            Assert.AreEqual("Bus", result);
        }

        [TestMethod]
        public void GetEncounterVehicleType_Taxi()
        {
            var newCase = new Case() { CaseId = 10 };
            var taxiEncounterGUID = "GUID3";

            var result = _target.GetEncounterTypeFromGUID(taxiEncounterGUID);

            Assert.AreEqual("Taxi", result);
        }

        [TestMethod]
        public void GetEncounterVehicleType_Goods()
        {
            var newCase = new Case() { CaseId = 10 };
            var goodsEncounterGUID = "GUID4";

            var result = _target.GetEncounterTypeFromGUID(goodsEncounterGUID);

            Assert.AreEqual("Goods", result);
        }
    }
}

