﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cranmore.Briefcase.Repo.Models;
using Cranmore.Briefcase.Repo.Services;
using Newtonsoft.Json;

namespace Cranmore.LiveQ.CaseCreator
{
    public interface ICaseCreator
    {
        Case CreateNewCase(WorkflowActionObject actionObject);

        void LinkCases(int caseId, int linkedCaseId);
    }

    public class CaseCreator : ICaseCreator
    {
        private ICaseTypeService _caseTypeService;
        private ICaseService _caseService;
        private ICaseIdGenerationService _caseIdGeneratorService;
        private ICaseLinkService _caseLinkService;
        private readonly int employeeId = 30;

        public CaseCreator(ICaseTypeService caseTypeService, ICaseService caseService, ICaseIdGenerationService caseIdGeneratorService,
            ICaseLinkService caseLinkService)
        {
            _caseTypeService = caseTypeService;
            _caseService = caseService;
            _caseIdGeneratorService = caseIdGeneratorService;
            _caseLinkService = caseLinkService;
        }

        public Case CreateNewCase(WorkflowActionObject actionObject)
        {
            var caseType = _caseTypeService.GetCaseTypeById(actionObject.NewCaseTypeId);
            var tempCaseId = GetNewCase(caseType);
            Case Case = _createNewCaseModel(caseType, actionObject, tempCaseId);
            _caseService.Add(Case);

            if (caseType.AllowAutoCaseIdGeneration)
            {
                _caseIdGeneratorService.RemoveCase(tempCaseId);
            }

            return Case;
        }

        private Case _createNewCaseModel(CaseType caseType, WorkflowActionObject actionObject, TempCaseIdentifier tempCaseId)
        {
            Case Case = new Case()
            {
                CaseName = tempCaseId.fullId,
                CaseTypeId = 55,
                EmployeeId = employeeId,
                DateCreated = DateTime.Now,
                LastModified = DateTime.Now,
                CaseStatus = actionObject.NewCaseStatus
            };

            return Case;
        }


        private TempCaseIdentifier GetNewCase(CaseType caseType)
        {
            if (!caseType.AllowAutoCaseIdGeneration) return null;

            return _caseIdGeneratorService.GetNewCase(caseType.CaseIdPattern);
        }

        public void LinkCases(int caseId, int linkedCaseId)
        {
            var linked = _caseLinkService.FindCaseLinkByIds(caseId, linkedCaseId);
            if (linked.Count != 0) return;

            CreateOneWayCaseLink(caseId, linkedCaseId);
            CreateOneWayCaseLink(linkedCaseId, caseId);
        }

        private void CreateOneWayCaseLink(int caseId, int linkedCaseId)
        {
            var cl = new CaseLinks
            {
                CaseId = caseId,
                LinkedCaseId = linkedCaseId,
                Notes = string.Empty,
                superceeded = false,
                DateCreated = DateTime.Now,
                EmployeeId = employeeId
            };
            _caseLinkService.Add(cl);
        }
    }

    public class WorkflowActionObject
    {
        [JsonProperty(PropertyName = "Action")]
        public string Action { get; set; }
        [JsonProperty(PropertyName = "NewCaseTypeId")]
        public int NewCaseTypeId { get; set; }
        [JsonProperty(PropertyName = "NewCaseStatus")]
        public string NewCaseStatus { get; set; }

        //Two different objects for different tasks?
        [JsonProperty(PropertyName = "LinkedCaseTypeId")]
        public int LinkedCaseTypeId { get; set; }
        [JsonProperty(PropertyName = "LinkedCaseStatus")]
        public string LinkedCaseStatus { get; set; }
    }
}