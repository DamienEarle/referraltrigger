﻿using Cranmore.Briefcase.Repo.Models;
using Cranmore.Briefcase.Repo.Services;
using Cranmore.LiveQ.ProcessNotification.DTOs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cranmore.LiveQ.NotificationProcessor
{
    public class NotificationProcessor
    {
        private ISchemaService _schemaService;
        private ICaseService _caseService;
        private ICaseDataService _caseDataService;
        private IFormsService _formService;
        private ICaseLinkService _caseLinkService;
        private ICaseSchemasService _caseSchemaservice;
        private ICaseTypeService _caseTypeService;
        private ICaseIdGenerationService _caseIdGeneratorService;
        private CaseCreator.CaseCreator _caseCreator;

        public NotificationProcessor(ICaseService caseService, ICaseDataService caseDataService, IFormsService formService, ICaseLinkService caseLinkService,
                ICaseSchemasService caseSchemaService, ICaseTypeService caseTypeService, ICaseIdGenerationService caseIdGeneratorService)
        {
            _caseService = caseService;
            _caseDataService = caseDataService;
            _formService = formService;
            _caseLinkService = caseLinkService;
            _caseSchemaservice = caseSchemaService;
            _caseTypeService = caseTypeService;
            _caseIdGeneratorService = caseIdGeneratorService;

            _schemaService = new Briefcase.Repo.Services.SchemaService();
            _caseCreator = new CaseCreator.CaseCreator(_caseTypeService, _caseService, _caseIdGeneratorService, _caseLinkService);
        }

        public void Process(NotificationRequest request)
        {
            var requestDetails = JsonConvert.DeserializeObject<OCAssessmentRequest>(request.RequestJSON);
        } 
    }
}
