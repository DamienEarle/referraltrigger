﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cranmore.LiveQ.NotificationProcessor.DTOs
{
    public class OCAssessmentRequest
    {
        [JsonProperty("OperatorName")]
        public string OperatorName { get; set; }
        [JsonProperty("TradingNameAtAddress")]
        public string TradingName { get; set; }
        [JsonProperty("UsedByOtherOperators")]
        public bool SharedWithOperators { get; set; }
        [JsonProperty("OtherOperators")]
        //TODO potentially a list
        public string SharedOperatorsNames{ get; set; }
        [JsonProperty("AddressLine1")]
        //TODO Break this out into multiline address
        public string Address { get; set; }
    }
}
