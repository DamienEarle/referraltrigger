﻿using Cranmore.DVA.LiveQ.ReferralTrigger.Services;
using Cranmore.Briefcase.Repo.Interfaces;
using Cranmore.Briefcase.Repo.Models;
using Cranmore.Briefcase.Repo.Services;
using Cranmore.LiveQ.SchemaService;
using ReferralTrigger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;
using System.Configuration;

namespace Cranmore.DVA.LiveQ.TriggerConsole
{
    class Program
    {
        private static IUnityContainer container;
        private static ReferralTrigger.ReferralTrigger trigger;
        private static Cranmore.LiveQ.SchemaService.SchemaService triggerSchemaService;
        private static int createRegulatoryCaseFormId = 77;
        private static string _JSON;

        static void Main(string[] args)
        {
            RegisterDependencies();
            InitializeDependencies();
            
            var operations = trigger.GetCompletedOperations();
            ProcessOperations(operations);
        }

        private static void ProcessOperations(List<Case> operations)
        {
            foreach (var operation in operations)
            {
                ProcessOperation(operation);
            }
        }

        private static void ProcessOperation(Case operation)
        {
                var operationLhCase = trigger.GetOperationLhCase(operation);
                var encounterDtos = GetEncounterDTOsFromOperation(operationLhCase);

                foreach (var encounterDto in encounterDtos)
                {
                    TriggerNewReferral(encounterDto);
                }

                if (encounterDtos.Any())
                    trigger.UpdateOperationStatus(operation);
        }


        private static List<EncounterCaseDTO> GetEncounterDTOsFromOperation(lh_Cases operationLhCase)
        {
            if (operationLhCase == null)
                return new List<EncounterCaseDTO>();

            var encounterGuids = trigger.GetEncounterGUIDSFromOperation(operationLhCase);
            var encounterDtos = trigger.BuildEncounterDTOsFromLookupGuids(encounterGuids);
            return filterCases(encounterDtos);
        }

        private static List<EncounterCaseDTO> filterCases(List<EncounterCaseDTO> encounterDtos)
        {
            encounterDtos = trigger.FilterLhCasesWhereComplete(encounterDtos);
            encounterDtos = trigger.FilterLhCasesWithInfringement(encounterDtos);
            return encounterDtos;
        }

        private static void TriggerNewReferral(EncounterCaseDTO encounterDto)
        {
            var referralCase = trigger.CreateNewReferralCase();
            triggerSchemaService.LinkEncounterToNewCase(referralCase.CaseId, encounterDto.lhCaseId);

            var JSON = PopulateJSON(encounterDto);
            triggerSchemaService.PopulateCaseData(referralCase, JSON);
        }

        private static string PopulateJSON(EncounterCaseDTO encounterDtos)
        {
            var keyValueMap = new Dictionary<string, string>()
            {
                { "3", "Referral" },
                { "2", encounterDtos.encounterType},
                { "5", DateTime.Now.ToShortDateString()}
            };
            
            return triggerSchemaService.PopulateJSONForm(keyValueMap, _JSON);
        }


        private static void RegisterDependencies()
        {
            container = new UnityContainer();
            RegisterServices();
            RegisterRepositories();
        }

        private static void RegisterServices()
        {
            container.RegisterType<ICaseService, CaseService>();
            container.RegisterType<ICaseLinkService, CaseLinkService>();
            container.RegisterType<Ilh_CasesService, lh_CasesService>();
            container.RegisterType<ICaseTypeService, CaseTypeService>();
            container.RegisterType<ICaseCreator, CaseCreator>();
            container.RegisterType<ICaseIdGenerationService, CaseIdGenerationService>();
            container.RegisterType<IVRMLookupService, VRMLookupService>();
            container.RegisterType<IInfringementActionService, InfringementActionService>();
            container.RegisterType<IFormsService, FormsService>();
            container.RegisterType<ICaseDataService, CaseDataService>();
            container.RegisterType<ISchemaService, Briefcase.Repo.Services.SchemaService>();
            container.RegisterType<ILHCaseDataService, lh_CaseDataService>();
            container.RegisterType<ICaseSchemasService, CaseSchemasService>();
        }

        private static void RegisterRepositories()
        {
            container.RegisterType<IRepository<CaseLinks>, Repository<CaseLinks>>();
            container.RegisterType<IRepository<Case>, Repository<Case>>();
            container.RegisterType<IRepository<TempCaseIdentifier>, Repository<TempCaseIdentifier>>();
            container.RegisterType<IRepository<CaseType>, Repository<CaseType>>();
            container.RegisterType<IDataRepository<Tbl_fn_VRM_Lookup_Vehicles>, DataRepository<Tbl_fn_VRM_Lookup_Vehicles>>();
            container.RegisterType<IDataRepository<Tbl_fn_Infringements_Infringement_Actions>, DataRepository<Tbl_fn_Infringements_Infringement_Actions>>();
            container.RegisterType<IRepository<lh_Cases>, Repository<lh_Cases>>();
            container.RegisterType<IRepository<CaseSchema>, Repository<CaseSchema>>();
            container.RegisterType<IRepository<Forms>, Repository<Forms>>();
            container.RegisterType<IRepository<CaseData>, Repository<CaseData>>();
            container.RegisterType<IRepository<lh_CaseData>, Repository<lh_CaseData>>();

            container.RegisterType<BriefcaseEntities, BriefcaseEntities>();
        }

        private static void InitializeDependencies()
        {
            trigger = container.Resolve<ReferralTrigger.ReferralTrigger>();
            triggerSchemaService = container.Resolve<Cranmore.LiveQ.SchemaService.SchemaService>();
            Company currentCompany = new Company() { CompanyId = 15 };
            Employee currentEmployee = new Employee() { EmployeeId = 30 };

            triggerSchemaService.SetUpSchemaService(ConfigurationManager.ConnectionStrings["CustomerData"].ConnectionString,
                ConfigurationManager.AppSettings["ManagementDB"], currentEmployee, currentCompany);

            _JSON = triggerSchemaService.LoadFormJSON(createRegulatoryCaseFormId);
        }
    }
}
