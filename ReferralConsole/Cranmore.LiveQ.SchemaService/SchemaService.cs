﻿using Cranmore.Briefcase.Repo.Helpers;
using Cranmore.Briefcase.Repo.Models;
using Cranmore.Briefcase.Repo.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cranmore.LiveQ.SchemaService
{
    public class SchemaService
    {
        private ISchemaService _schemaService;
        private ICaseService _caseService;
        private ICaseDataService _caseDataService;
        private IFormsService _formService;
        private ICaseLinkService _caseLinkService;
        private ICaseSchemasService _caseSchemaservice;

        private Employee _currentEmployee;
        private Company _currentCompany;

        public SchemaService(ICaseService caseService, ICaseDataService caseDataService, IFormsService formService, ICaseLinkService caseLinkService, 
            ICaseSchemasService caseSchemaService)
        {

            _caseService = caseService;
            _caseDataService = caseDataService;
            _formService = formService;
            _caseLinkService = caseLinkService;
            _caseSchemaservice = caseSchemaService;

            _schemaService = new Briefcase.Repo.Services.SchemaService();
        }

        public void SetUpSchemaService(string customerDataConnection, string managementdbString, Employee employee, Company company)
        {
            _schemaService.setUpSchemaService(customerDataConnection, managementdbString, employee, company);
        }

        public string LoadFormJSON(int formId)
        {
            return _caseSchemaservice.GetCaseSchemaById(formId).JSON;
        }

        public void LinkEncounterToNewCase(int CaseId, int lhCaseId)
        {
            var newCaseLink = new CaseLinks() {
                CaseId = CaseId,
                LinkedLhCaseId = lhCaseId,
                DateCreated = DateTime.Now
            };

            _caseLinkService.Add(newCaseLink);
        }

        public string PopulateJSONForm(Dictionary<string, string> keyValueMap, string json)
        {
            var jsonObjects = JsonConvert.DeserializeObject<List<dynamic>>(json);
            foreach(var item in jsonObjects)
            {
                if(keyValueMap.ContainsKey(item["id"].Value.ToString()))
                {
                    var newValue = keyValueMap.FirstOrDefault(a => a.Key == item["id"].Value.ToString()).Value;
                    item.value = newValue;
                }
            }

            return JsonConvert.SerializeObject(jsonObjects);
        }

        public void PopulateCaseData(Case Case, string data)
        {
            _schemaService.setupJSONForm(data, Case.CaseId);
            int CaseSchemaId = _schemaService.getCaseSchemaId();
            string formName = _schemaService.getFormName();

            _schemaService.setOptionsForValidation(null);
            _schemaService.setSourceJSONForValidation(data);
            _schemaService.updateData();
            
            SaveJSON(Case.CaseId, CaseSchemaId, data);
        }

        private void SaveJSON(int caseId, int schemaId, string JSON)
        {
            var caseData = _caseDataService.GetByCaseDataAndSchemaId(caseId, schemaId);

            caseData = new CaseData()
            {
                JSON = JSON,
                CaseId = caseId,
                SchemaId = schemaId,
                LastModified = DateTime.Now,
            };

            caseData.DateCreated = caseData.LastModified;
            _caseDataService.Add(caseData);
        }
    }
}
